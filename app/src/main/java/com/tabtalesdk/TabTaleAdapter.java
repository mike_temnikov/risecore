package com.tabtalesdk;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;


import com.tabtale.publishingsdk.core.Analytics;
import com.tabtale.publishingsdk.core.PublishingSDKErrors;
import com.tabtale.publishingsdk.core.ServiceManager;
import com.tabtale.publishingsdk.core.StartupDelegate;
import com.tabtale.publishingsdk.services.AnalyticsDelegate;
import com.tabtale.publishingsdk.services.BannersDelegate;
import com.tabtale.publishingsdk.services.ConfigurationDelegate;
import com.tabtale.publishingsdk.services.LocationMgrAttributes;
import com.tabtale.publishingsdk.services.LocationMgrDelegate;
import com.tabtale.publishingsdk.services.RewardedAdsDelegate;
import com.tabtale.publishingsdk.services.SplashDelegate;
import com.tabtale.publishingsdk.services.WebViewDelegate;

import org.json.JSONObject;

/**
 * Created by miket on 06.02.2017.
 */

public class TabTaleAdapter {

    public static TabTaleAdapter i;

    private static final String TAG = "PSDKExample";
    public static final String MOREAPPS_LOCATION_NAME = "moreApps";
    public static final String SCENETRANSITIONS_LOCATION_NAME = "sceneTransitions";
    public static final String SESSION_START_LOCATION_NAME = "sessionStart";

    public TabTaleAdapter(){
        i = this;
    }


    private Boolean checkServiceManager(){
        return ServiceManager.instance() != null && ServiceManager.instance().getAppLifeCycleMgr() != null;
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(checkServiceManager()){
            ServiceManager.instance().getAppLifeCycleMgr().onActivityResult(requestCode,resultCode,data);
        }
    }


	public void onShowLocation(String locationName) {
        //MOREAPPS_LOCATION_NAME
        //SCENETRANSITIONS_LOCATION_NAME
		if (checkServiceManager() && locationName != null) {
				showLocation(locationName);
		}
	}


    private void showLocation(String locationName)
    {
        //you must call reportLocation() before isLocationReady()
        ServiceManager.instance().getLocationMgr().reportLocation(locationName);

        long result = ServiceManager.instance().getLocationMgr().isLocationReady(locationName);
        if (result == LocationMgrAttributes.LOCATION_MGR_ATTR_NO_SOURCE) {
            //if location is not ready for any reason, this is the result you will receive
        } else {
            //LocationMgrAttributes.LOCATION_MGR_ATTR_SOURCE_EXIST - if location is ready, but you should not pause/stop the game music, this is the result you will receive
            //LocationMgrAttributes.LOCATION_MGR_ATTR_PLAYING_MUSIC - if location is ready, and you should pause/stop game music, this is the result you will receive
            ServiceManager.instance().getLocationMgr().show(locationName);
        }
    }

    public void onGameOver() {
        onShowLocation(SCENETRANSITIONS_LOCATION_NAME);
        onShowBanner();
    }

    public void onShowBanner() {
        if (ServiceManager.instance() != null && ServiceManager.instance().getBanners() != null) {
            ServiceManager.instance().getBanners().show();
        }
    }

    public void onHideBanner() {
        if (ServiceManager.instance() != null && ServiceManager.instance().getBanners() != null) {
            ServiceManager.instance().getBanners().hide();
        }
    }

    public void onShowReward()
    {
        if (ServiceManager.instance() != null && ServiceManager.instance().getRewardedAdsService() != null) {
            ServiceManager.instance().getRewardedAdsService().showAd();
        }
    }

    public void initServiceManager(Activity activity){
        ServiceManager.setAnalyticsDelegate(new AnalyticsDelegate() {
            @Override
            public void onRequestEngagementComplete(String decisionPoint, JSONObject parameters) {

            }
        });

        ServiceManager.setConfigurationDelegate(new ConfigurationDelegate() {
            @Override
            public void onConfigurationLoaded() {

            }
        });

        ServiceManager.setSplashDelegate(new SplashDelegate() {
            @Override
            public void onSplashAdded() {

            }

            @Override
            public void onSplashRemoved() {

            }
        });

        ServiceManager.setWebViewDelegate(new WebViewDelegate() {
            @Override
            public void onPlaySound(String soundFilePath) {

            }

            @Override
            public void onStartAnimationEnded() {

            }
        });

        ServiceManager.setRewardedAdsDelegate(new RewardedAdsDelegate() {
            @Override
            public void adIsReady() {

            }

            @Override
            public void adIsNotReady() {

            }

            @Override
            public void adWillShow() {

            }

            @Override
            public void adDidClose() {

            }

            @Override
            public void adShouldReward() {
                Log.v(TAG,"Rewarded Ads Service : User will be rewarded for watching the video");
            }

            @Override
            public void adShouldNotReward() {
                Log.v(TAG,"Rewarded Ads Service : User will not be rewarded for watching the video");
            }
        });

        ServiceManager.setBannersDelegateAndLayout((ViewGroup)activity.getWindow().getDecorView().getRootView(), new BannersDelegate() {
            @Override
            public void onBannerShown() {
            }

            @Override
            public void onBannerHidden() {
            }

            @Override
            public void onBannerFailed() {
            }

            @Override
            public void onBannerConfigurationUpdate() {

            }
        });

        ServiceManager.setLocationMgrDelegate(new LocationMgrDelegate() {
            @Override
            public void onLocationLoaded(String location, long attribute) {
                switch(location){
                    default:
                        break;
                    case MOREAPPS_LOCATION_NAME:

                        break;
                    case SCENETRANSITIONS_LOCATION_NAME:

                        break;
                }
            }

            @Override
            public void onLocationFailed(String location, PublishingSDKErrors error) {
                switch(location){
                    default:
                        break;
                    case MOREAPPS_LOCATION_NAME:

                        break;
                    case SCENETRANSITIONS_LOCATION_NAME:

                        break;
                }
            }

            @Override
            public void onShown(String location, long attribute) {

            }

            @Override
            public void onShowFailed(String location, long attribute) {

            }

            @Override
            public void onClosed(String location, long attribute) {

            }

            @Override
            public void onConfigurationLoaded() {

            }

            @Override
            public void onRequiredAction(Action action, Object object) {

            }
        });

        ServiceManager sm = ServiceManager.start(activity, new StartupDelegate() {
            @Override
            public void onConfigurationReady() {

            }

            @Override
            public void onPSDKReady() {
                Log.v(TAG,"onPSDKReady");
                //the location name "sessionStart" has special handling in the psdk and should be called after onPSDKReady is called. (not onLocationLoaded)
                showLocation(SESSION_START_LOCATION_NAME);
            }
        },"en");
        if(sm == null){
            Log.e(TAG,"Error! Service Manager did not initiate correctly. The PSDK will not work when this happens. Usually this happens because of bad configuration, check psdk.json and previous logs.");
        }
        else {
            //you must report appIsReady() (when your app is loaded and ready to run) to the psdk for it to work correctly.
            sm.getAppLifeCycleMgr().appIsReady();

            //analytics service example
            if(sm.getAnalytics() != null){
                JSONObject eventParams = new JSONObject();
                sm.getAnalytics().logEvent(Analytics.ANALYTICS_TARGET_FLURRY | Analytics.ANALYTICS_TARGET_TT_ANALYTICS,"eventName",eventParams,false);
                //timed events are ended when endLogEVent is sent with the same event name as a logEvent with timed argument set to true.
                sm.getAnalytics().logEvent(Analytics.ANALYTICS_TARGET_FLURRY | Analytics.ANALYTICS_TARGET_TT_ANALYTICS,"timedEvent",eventParams,true);
                sm.getAnalytics().endLogEvent("timedEvent",null);
            }
        }
    }

    public void onStart() {
        if(checkServiceManager()){
            ServiceManager.instance().getAppLifeCycleMgr().onStart();
        }
    }

    public void onResume() {
        if(checkServiceManager()){
            ServiceManager.instance().getAppLifeCycleMgr().appIsReady();
            ServiceManager.instance().getAppLifeCycleMgr().onResume();
        }
    }

    public void onPause() {
        if(checkServiceManager()){
            ServiceManager.instance().getAppLifeCycleMgr().onPaused();
        }
    }

    public void onDestroy() {
        if(checkServiceManager()){
            ServiceManager.instance().getAppLifeCycleMgr().onDestroy();
        }
    }

    public void onBackPressed() {
        if(checkServiceManager()){
            ServiceManager.instance().getAppLifeCycleMgr().onBackPressed();
        }
    }
}
